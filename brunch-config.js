exports.files = {
  javascripts: {
    joinTo: 'app.js'
  },
  stylesheets: {
    joinTo: 'app.css'
  }
}

exports.plugins = {
  sass: {
    mode: 'native',
    sourceMapEmbed: true,
    precision: 8
  },
  uglify: {
    mangle: true,
    compress: {
      global_defs: {
        ENV: 'production'
      }
    }
  },
  cleancss: {
    keepSpecialComments: 0,
    removeEmpty: true
  }
}
